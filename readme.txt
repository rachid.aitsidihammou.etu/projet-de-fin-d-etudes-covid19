Installation de l'application
Talend
1) Sur Talend, click droit sur Job, puis importer des element
2)Chercher le dossier nommé pfe-covid19-talend(existant dans le rendu)
3)Cocher tous les jobs, puis ok
4)Changer le chemin des fichiers  dans les composants d'entrée 
pour tous les jobs sauf celui de F_COVID19(les fichiers sont dans le repertoire fichiers)
5)Executer tous les jobs en laissant Job_Covid19 en dernier.
 
Power BI

1)Ouvrir Power BI
2)Click sur obtenir les données, puis sur plus, ensuite sur autres
3)Click sur ODBC puis se connecter 
4)une fenetre s'ouvre pour selectionner les tables

On peut acceder au rapport directement dans le repertoire tBords-powerBI existant dans le rendu.


Pour éviter l'execution surr talend, executer le script SQL existant dans le repertoire script-bd-oracle,
et là on aller directement sur Power BI et charger les tables, ou bien aller directement ouvrir les rapport 
existant dans le dossier tBords-powerBI.